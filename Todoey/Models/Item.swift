//
//  Item.swift
//  Todoey
//
//  Created by Leah Joy Ylaya on 12/16/20.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation

class Item: Encodable, Decodable {
    var title: String = ""
    var done: Bool = false
}
